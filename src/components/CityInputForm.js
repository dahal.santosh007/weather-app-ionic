import React, { useState,useEffect } from 'react';
import fetchAction from '../actions/fetchDataAction';
import {connect} from 'react-redux';
import Clock from 'react-live-clock';
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";

import { 
        IonContent, 
        IonGrid, 
        IonCol, 
        IonInput, 
        IonRow, 
        IonButton,
        IonModal,
        IonImg} from '@ionic/react';
import '../theme/App.css'

const CityInputForm =({city,fetchAction,weather,condition})=>{
    const [enteredCity,setEnterCity] = useState('Boston,US');
    const [showModal, setShowModal] = useState(false);

    const myStyle = `${condition.main === 'Clouds' ? 
                            'primary' : 
                            condition.main === 'Rain'? 
                            'secondition.mainary':
                            condition.main === 'Mist'?
                            'tertiary':
                            condition.main === 'Snow'?
                            'success':
                            condition.main === 'Thunderstorm'?
                            'warning':
                            'danger'}`;

    useEffect(()=>{
        fetchAction({city:enteredCity});
        console.log(process.env.REACT_APP_API_KEY);
    },[])

    const submitCity =(e)=>{
        e.preventDefault();
        fetchAction({city:enteredCity})
    }

    const clickedAdd=(e)=>{
        e.preventDefault();
        console.log('entered');
    }

    return( 
        !!!condition.main ? "":
        <IonContent color={myStyle}>
            <IonGrid>
                <IonRow className="header-row">
                    <IonCol size="2">
                    </IonCol>
                    <IonCol size="8">
                        <form onSubmit={submitCity}>
                            <IonInput   type="text" 
                                        spellcheck = {true} 
                                        value={enteredCity} 
                                        onIonChange={e=>setEnterCity(e.target.value)}
                                        className="input-box"/>
                        </form>
                    </IonCol>
                    <IonCol size="2">
                        <IonButton className = "btn-addcity" color="dark" onClick={() => setShowModal(true)}> 
                            <h6 className="add-city">+</h6>
                        </IonButton>
                        <IonModal isOpen={showModal}>
                            <p>This is modal content</p>
                            <IonButton onClick={() => setShowModal(false)}>Close Modal</IonButton>
                        </IonModal>
                    </IonCol>
                </IonRow>

                <IonRow>
                    <IonCol size="12">
                        <Clock format="h:mm A" interval={1000} ticking={true} className="reg-time"/>
                    </IonCol>
                </IonRow>

                <IonRow className="weather-condition-row">
                    <IonCol size = "5" className="condition-icon-col">
                        <IonImg className = "condition-icon" src={require(`/Users/user/Documents/Project/weather-app-ionic/src/icons/${condition.main}.svg`)} />
                    </IonCol>
                    <IonCol size = "6" className="condition-text-col">
                        <h1 className = "condition-text">
                            {condition.main}
                        </h1>
                    </IonCol>
                </IonRow>

                <IonRow className="temp-row">
                    <IonCol size="7" className='main-temp ion-no-padding'>
                        <h2 className = "temp-text">{(weather.temp).toFixed(0)}</h2>
                    </IonCol>
                    <IonCol size="1" className="ion-no-padding">
                        <h6 className = "degree-symbol">
                            &deg;
                        </h6>
                    </IonCol>
                    <IonCol size="3" className="ion-no-padding">
                        <IonGrid>
                            <IonRow className="max-min-row">
                                <IonCol size="12" className="max-temp">
                                    {(weather.temp_max).toFixed(0)}&deg; F
                                </IonCol>
                                <IonCol size="12" className="min-temp">
                                    {(weather.temp_min).toFixed(0)}&deg; F
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCol>
                </IonRow>

                <IonRow>
                    <IonCol size="12" className="day-date-col">
                        <DayPicker selectedDays={new Date()}/>
                    </IonCol>
                </IonRow>
            </IonGrid>
        </IonContent>
    )
}

const mapStateToProps = state =>({
    weather : state.posts.weatherData,
    condition: state.posts.conditionData
})

export default connect(mapStateToProps,{fetchAction})(CityInputForm);